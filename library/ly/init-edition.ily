\version "2.17.3"

#(ly:set-option 'relative-includes #t)
#(define-public editionInitialized #t)

\include "layout/global-layout.ily"

% Select whether to keep or discard the line
% and page breaks of the original score
% (may be overwritten individually for each part)
\include "layout/keep-original-breaks.ily"
%\include "layout/discard-original-breaks.ily"

\include "global/headers.ily"

% Include markup styles
\include "styles/markups.ily"
