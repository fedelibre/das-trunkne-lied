library/ly/to-lilylib
---

This folder contains library items that are intended to be included in lilylib.

So when there is an agreement to include it there, it has to be removed here, 
and (if necessary) any updates to the usage syntax have to be applied.