%{
   library/ly/layout/global-layout
   
   This file contains layout settings 
   that are global to the project,
   i.e. that affect both score and parts etc.
%}

#(set-global-staff-size 17)

\include "to-lilylib/engravers.ily"

\layout {
  \context {
    \Staff
    % Each segment of the score is started with an explicit
    % time signature, regardless of the previous time signature.
    % Therefore redundant time signatures should be be suppressed.
    \consists #suppressRedundantTimeSig
  }
  \context { 
    \Score
    % We want time signature changes highlighted by double barlines
    \consists #doubleBarlinesAfterTimeSig
    
    % Style of rehearsal marks
    markFormatter = #format-mark-box-numbers
    
  }
}