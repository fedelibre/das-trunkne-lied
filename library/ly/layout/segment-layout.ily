\header {
  instrument = \markup { \italic { Preview of single segment } }
}

\paper {
  ragged-right = ##t
}

% Set flag to be used in compileSegment
isSegment = ##t

% Just mag origBreaks to default ones (i.e. no fancy numberings)
origBreak = \break

origLineBreak = \break

