%{
   Include default settings, library, and a specific layout file
   but only if we're compiling a segment file
   (determined by the lacking definition of 'editionInitialized
    which is set the first time initEdition.ily is read)
%}
includeDefaults = 
#(define-void-function (parser location)()
   (if (not (defined? 'editionInitialized))
    (ly:parser-parse-string (ly:parser-clone parser) "
       \\include \"init-edition.ily\"
       \\include \"layout/segment-layout.ily\"")))
\includeDefaults

%{
   Compile a music segment to a score.
   This happens only when we're compiling one of the
   segment .ily files
%}

compileSegment = 
#(define-void-function (parser location segment)
   (ly:music?)
   ; don't do anything if we're not compiling a segment file
   (if (defined? 'isSegment)
       (let ((book 
               #{ \book { \score { 
                             \new Staff \new Voice {
                               \bar ""
                               $segment
                             } } } #} ))
         (ly:book-process book #{ \paper {} #} #{ \layout {} #} (ly:parser-output-name parser))
         ); close let
   ); close if condition
) % end function
