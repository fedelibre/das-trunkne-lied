trunkenes-lied
==============

Oskar Fried: Das Trunkene Lied

---

New edition of Oskar Fried "Das Trunkene Lied",  
commissioned by: [„das junge orchester NRW“](www.djo-nrw.de)

---

"Installation":

- Download or clone the complete repository  
(Cloning is highly recommended)
- You need a current development version of [GNU LilyPond](http://www.lilypond.org) to compile the score and parts
- Make sure that the subdirectory library/ly is in LilyPond's search path

---

Contact:  
Urs Liska <mailto:beautifulscores@ursliska.de>