%{
  parts/violoncello/39.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 39 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XXXIX = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 12/8
  R1.*2 \origBreak |%59
  R1.*2 \origBreak |%60
  \time 6/8 R2.
  \mark \default
}

\compileSegment \XXXIX
