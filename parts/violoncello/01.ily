%{
  parts/violoncello/01.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 01 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

I = \relative es, {
  \set Staff.instrumentName = "Violoncello."
  \set Staff.shortInstrumentName = "Vc."
  \key ges \major % Replace with actual key
  \clef bass % Replace with actual clef
  \time 3/4
  es2(\p\<^\markup "mit Dämpfer." ges4 | %1
  f2.)\> | %2
  R2.*8\! \origBreak %3 [these numbers indicate the number of the previous page]
  \mark \default
}

\compileSegment \I
