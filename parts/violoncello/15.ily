%{
  parts/violoncello/15.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 15 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XV = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*2 \origBreak | %17
  \time 5/4 R4*5 \time 4/4 R1
  \mark \default
}

\compileSegment \XV
