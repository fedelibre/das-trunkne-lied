%{
  parts/violoncello/09.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 09 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

IX = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4 R2. \origBreak | %10
  R2.*3 \time 5/4 R4*5
  \mark \default
}

\compileSegment \IX
