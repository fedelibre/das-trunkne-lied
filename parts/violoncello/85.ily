%{
  parts/violoncello/85.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 85 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LXXXV = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4 R2.*4 \origBreak |%138
  R2.*4 \origBreak |%139
  R2.*2
  \mark \default
}

\compileSegment \LXXXV
