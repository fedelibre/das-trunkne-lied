%{
  parts/violoncello/14.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 14 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XIV = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*5 \origBreak | %16
  R1*4 \mark \default
}

\compileSegment \XIV
