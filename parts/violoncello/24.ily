%{
  parts/violoncello/24.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 24 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XXIV = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*5 \origBreak | %31
  R1*5
  \mark \default
}

\compileSegment \XXIV
