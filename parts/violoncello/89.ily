%{
  parts/violoncello/89.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 89 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LXXXIX = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*7 \origBreak |%145
  \mark \default
}

\compileSegment \LXXXIX
