%{
  parts/violoncello/47.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 47 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XLVII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4 R2. \time 4/4 R1 \origBreak |%74
  \time 3/4 R2.*8 \origBreak |%75
  R2.
  \mark \default
}

\compileSegment \XLVII
