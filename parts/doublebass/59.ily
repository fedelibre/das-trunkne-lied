%{
  parts/doublebass/59.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 59 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

LIX = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*4 \origBreak |%96
  R1*4
  \mark \default
}

\compileSegment \LIX
