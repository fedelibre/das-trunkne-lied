%{
  parts/doublebass/12.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 12 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XII = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4 R1*5 \origBreak | %15
  \mark \default
}

\compileSegment \XII
