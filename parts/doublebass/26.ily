%{
  parts/doublebass/26.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 26 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XXVI = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4 R2.*5 \origBreak | %37
  R2.*5
  \mark \default
}

\compileSegment \XXVI
