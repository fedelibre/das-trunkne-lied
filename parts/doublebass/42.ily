%{
  parts/doublebass/42.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 42 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XLII = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 12/8
  R1.*2  \origBreak |%66
  \time 9/8 R8*9 \time 12/8 R1. \origBreak |%67
  \mark \default
}

\compileSegment \XLII
