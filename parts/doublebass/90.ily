%{
  parts/doublebass/90.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 90 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XC = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*5 \bar "|." |%146
}

\compileSegment \XC
