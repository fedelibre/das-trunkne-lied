%{
  parts/doublebass/18.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 18 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XVIII = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*3 \origBreak | %22
  R1
  \mark \default
}

\compileSegment \XVIII
